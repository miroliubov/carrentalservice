<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Registration</title>
        <link rel="stylesheet" href="/css/style.css" type="text/css" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
          <script src="/js/jquery.maskedinput.min.js"></script>
    </head>
    <body>
    <div id="login-form">
        <h2>Регистрация</h2>
        <fieldset>
            <form action="/registration" method="post">
                <input type="email" class="placeholder" placeholder="E-mail" name="email">
                <input type="password" class="placeholder" placeholder="Пароль" name="password">
                <input type="text" class="placeholder" placeholder="Имя" name="name">
                <input type="text" class="placeholder" placeholder="Фамилия" name="surName">
                <input type="submit" value="ЗАРЕГИСТРИРОВАТЬСЯ">
            </form>
        </fieldset>
        <h3>Уже зарегистрированы? </h3>
        <div id="in">
        <a href="/login">Войти!</a>
        </div>
    </div>
    <c:forEach var="error" items="${errors}">

    			${error.defaultMessage}<br>

    	</c:forEach>
  </body>
</html>