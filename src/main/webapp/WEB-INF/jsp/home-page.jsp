<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Login</title>
        <link rel="stylesheet" href="../css/style.css" type="text/css" />
        <link rel="stylesheet" href="../css/reservationstyle.css" type="text/css" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    </head>
    <body>
        <div id="wrapper">
            <header>
                <a href="#"><img id="alogo" src="../img/cars/logo.png" alt="Car rental"></a>
                <input type="button" onclick="location.href='/logout'" value="Logout" >
                <input type="button" onclick="location.href='/status'" value="Статус заказа">
                <c:if test="${currentUserRole == 'ADMIN'}">
                    <input type="button" onclick="location.href='/admin'" value="Страница администратора">
                    <input type="button" onclick="location.href='/adminorder'" value="Заказы пользователей">
                </c:if>

                <div id="userName">
                <c:if test="${currentUserRole == 'USER'}">
                    <span>Здравствуйте, ${userName}</span>
                    <br>
                    <span>Баланс: ${userBalance}</span>
                </c:if>
                </div>

            </header>
		    <div id="content">
		        <div class="reservationForm">

                  <form action="/home" method="post" name="resForm" id="resForm">
		              <p>
                      <label for="">Выберите машину</label>
                       <br>

                        <form:select name="models" path="models">
                            <form:option value="NONE" label="Выберите машину" />
                            <form:options items="${models}" />
                        </form:select>

		                </p>
		                <p>
		                <label for="">Дата начала аренды</label>
		                <br>
		                <input type="date" id="date1" name="date1">
		                </p>
		                <p>
		                <label for="">Дата конца аренды</label>
		                <br>
		                <input type="date" id="date2" name="date2">
		                </p>
		                <p>
		                <label for="">Введите паспортные данные</label>
		                <br>
		                <input type="text" id="passport" name="passport">
		                </p>
		                <p>
		                <input type="submit" value="Забронировать">
		                </p>

		            </form>
		        </div>
		    </div>
	    </div>
    </body>
</html>