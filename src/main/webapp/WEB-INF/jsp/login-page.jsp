<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Login</title>
        <link rel="stylesheet" href="/css/style.css" type="text/css" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script type="text/javascript" src="/js/validate.js"></script>
    </head>
    <body>
    <div id="login-form">
        <h2>Авторизация</h2>
        <div id="msg"></div>
        <fieldset>
            <form name="UserEnter" id="UserEnter" action="/login" method="post">
                <input type="text" class="placeholder" placeholder="Email" id="email" name="email">
                <input type="password" class="placeholder" placeholder="Пароль" id="pass" name="password">
                <input type="button" value="ВОЙТИ" onclick="validateForm()" id="validate">
            </form>
        </fieldset>
        <h3>Нет аккаунта?</h3>
        <div id="in">
        <a href="/registration">Зарегистрироваться!</a>
        </div>
    </div>
  </body>
</html>