<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Login</title>
        <link rel="stylesheet" href="../css/style.css" type="text/css" />
        <link rel="stylesheet" href="../css/reservationstyle.css" type="text/css" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    </head>
    <body>
        <div id="wrapper">
            <header>
                <a href="#"><img id="alogo" src="../img/cars/logo.png" alt="Car rental"></a>
                <input type="button" onclick="location.href='/login'" value="Logout" >
                <input type="button" onclick="location.href='/home'" value="Главная страница">
                <input type="button" onclick="location.href='/admin'" value="Страница администратора">
            </header>
		    <div id="content">
		        <div class="reservationForm">
		            <table>
                        <tr>
                            <th>User ID</th>
                            <th>Автомобиль</th>
                            <th>Даты заказа</th>
                            <th>Статус</th>
                        </tr>
                        <c:forEach items="${orders}" var="order">
		                <tr>
		                    <td><c:out value="${order.userId}" /></td>
		                    <td><c:out value="${order.carId}" /></td>
		                    <td><c:out value="${order.start_date} - ${order.finish_date}" /></td>
		                    <td><c:out value="${order.status}" /></td>
		                </tr>
		                <tr>
                          <td>
                            <form action="/adminorder" method="post">
                                  <select name="status" id="status">

                                    <option disabled>Выберите статус</option>

                                    <option value="RETURN">Вернуть автомобиль</option>

                                    <option value="BROKEN">Автомобиль поврежден</option>

                                  </select>

                                  <textarea name="textrepair" cols="20" rows="2"></textarea>

                                  <input type="number" min="0" step="0.1" name="repair">

                                  <input type="hidden" name="orderId" value="${order.orderId}">

                                  <input type="submit" value="Подтвердить" name="confirm">
                            </form>
                            </td>
		                </tr>
		                </c:forEach>
		            </table>
		        </div>
		    </div>
	    </div>
    </body>
</html>