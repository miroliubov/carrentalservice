<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Login</title>
        <link rel="stylesheet" href="../css/style.css" type="text/css" />
        <link rel="stylesheet" href="../css/reservationstyle.css" type="text/css" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    </head>
    <body>
        <div id="wrapper">
            <header>
                <a href="#"><img id="alogo" src="../img/cars/logo.png" alt="Car rental"></a>
                <input type="button" onclick="location.href='/login'" value="Logout" >
                <input type="button" onclick="location.href='/home'" value="Главная страница">
                <div id="userName">
                    <span>Здравствуйте, ${userName}</span>
                    <br>
                    <span>Балланс: ${userBalance}</span>
                </div>
            </header>
		    <div id="content">
		        <div class="reservationForm">
		            <table>
                        <tr>
                            <th>Машина</th>
                            <th>Стоимость</th>
                            <th>Даты заказа</th>
                            <th>Статус</th>
                            <th>Кнопка</th>
                        </tr>
                        <c:forEach items="${orders}" var="order">
                            <c:if test = "${(order.status == 'WAIT') || (order.status == 'RESERVED')}">
                                <tr>
                                    <td><c:out value="${order.model}" /></td>
                                    <td><c:out value="${order.periodPrice}" /></td>
                                    <td><c:out value="${order.start_date} - ${order.finish_date}" /></td>
                                    <td><c:out value="${order.status}" /></td>
                                    <td>
                                        <form action="/status" method="post">
                                            <input type="hidden" value="${order.orderId}" name="orderId">
                                            <input type="submit" value="вернуть" name="receive">
                                        </form>
                                    </td>
                                </tr>
                            </c:if>
		                </c:forEach>
		            </table>
		            <table>
                        <tr>
                            <th>Машина</th>
                            <th>Стоимость ремонта</th>
                            <th>Сообщение</th>
                            <th>Кнопка</th>
                        </tr>
                        <c:forEach items="${repairOrders}" var="repairOrder">

                                <tr>
                                    <td><c:out value="${repairOrder.model}" /></td>
                                    <td><c:out value="${repairOrder.repairPrice}" /></td>
                                    <td><c:out value="${repairOrder.message}" /></td>
                                    <td>
                                        <form action="/repair" method="post">
                                            <input type="hidden" value="${repairOrder.orderId}" name="orderId">
                                            <input type="submit" value="оплатить" name="receive">
                                        </form>
                                    </td>
                                </tr>

                        </c:forEach>
                    </table>
		        </div>
		    </div>
	    </div>
    </body>
</html>