<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Login</title>
        <link rel="stylesheet" href="../css/style.css" type="text/css" />
        <link rel="stylesheet" href="../css/popupstyle.css" type="text/css" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="../js/modalview.js"></script>
        <script src="../js/sendcarform.js"></script>
    </head>
    <body>
        <div id="wrapper">
            <header>
                <a href="#"><img id="alogo" src="/img/cars/logo.png" alt="Car rental"></a>
                <input type="button" id="addCarPage" onclick="PopUpShow()" value="Добавить автомобиль">
                <input type="button" onclick="location.href='/home'" value="Главная страница">
                <input type="button" onclick="location.href='/adminorder'" value="Заказы пользователей">
            </header>
		    <div id="content">
		        <div class="item">

		        </div>
		    </div>
	    </div>

	    <div id="shadowbox">
            <div class="popupwin" id="popupwin">
                <a href="javascript:PopUpHide()" id="hidepopup">X</a>
                <form action="/admin" id="addcarform" name="addcarform" method="post">
                    <p>
                        <label for="model">Марка и модель автомобиля</label>
                        <br>
                        <input type="text" id="model" name="model">
                    </p>
                    <p>
                        <label for="price">Стоимость дня аренды</label>
                        <br>
                        <input type="text" id="price" name="price">
                    </p>
                    <p>
                        <label for="img">Изображение для автомобиля</label>
                        <br>
                        <select name="img">
                            <option value="/img/cars/audi-q5.jpg">Audi</option>
                            <option value="/img/cars/toyota-ae86.jpg">Toyota</option>
                            <option value="/img/cars/kia-rio.jpg">KIA</option>
                            <option value="/img/cars/hyundai-solaris.jpg">Hyundai</option>
                            <option value="/img/cars/lada-granta.jpg">Lada</option>
                            <option value="/img/cars/gla-250.jpg">Mercedes</option>
                        </select>
                    </p>
                    <p>
                        <input type="button" value="Добавить" onclick="sendcarform()" id="addCar">
                    </p>
                </form>
            </div>
        </div>

    </body>
</html>