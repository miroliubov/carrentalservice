function validateForm () {

    var valid = true;

    var email = document.getElementById('email');
    
    var email_regexp = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    
    if (!email_regexp.test(email.value)) {
        
        var err=document.getElementById('msg');
        
        err.style.cssText="text-align: center;\
            background-color: rgba(205, 92, 92, 0.8);\
            margin: 10px 10px 0px 10px;\
            padding: 5px;"
        
        err.innerHTML='Неверный логин или пароль';
        
        valid = false;
    }
    
    var password = document.getElementById('pass');
    
    if (password.value.length == 0) {
        
        var err=document.getElementById('msg');
        
        err.style.cssText="text-align: center;\
            background-color: rgba(205, 92, 92, 0.8);\
            margin: 10px 10px 0px 10px;\
            padding: 5px;"
        
        err.innerHTML='Неверный логин или пароль';
        
        valid = false;
    }

    if (valid) {
        document.getElementById("UserEnter").submit();
        }
   
}