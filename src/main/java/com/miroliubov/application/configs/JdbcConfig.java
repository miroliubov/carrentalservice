package com.miroliubov.application.configs;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

@Configuration
@ComponentScan(basePackages = {"application.service", "application.dao"})
public class JdbcConfig {

    @Bean
    public JdbcTemplate jdbcTemplate () {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://localhost:3306/rentaldb?useSSL=false&useLegacyDatetimeCode=false&serverTimezone=Europe/Samara");
        dataSource.setUsername("root");
        dataSource.setPassword("qazwsx");
        return new JdbcTemplate(dataSource);
    }

}
