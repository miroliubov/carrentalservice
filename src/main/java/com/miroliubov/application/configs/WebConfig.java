package com.miroliubov.application.configs;

import com.miroliubov.application.interceptor.AdminInterceptor;
import com.miroliubov.application.interceptor.AuthInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Bean
    public AuthInterceptor authInterceptor() {
        return new AuthInterceptor();
    }

    @Bean
    public AdminInterceptor adminInterceptor() {
        return new AdminInterceptor();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(authInterceptor()).addPathPatterns("/**")
                .excludePathPatterns("/login", "/registration", "/status",
                        "/logout", "/css/**", "/js/**", "/img/cars/**");

        registry.addInterceptor(adminInterceptor()).addPathPatterns("/adminorder", "/admin");

    }

}
