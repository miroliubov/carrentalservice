package com.miroliubov.application.dao;

import com.miroliubov.application.entity.User;

public interface IUserDao {

    void createUser(User user);

    User getUserByEmail(String email);

    void decreaseBalance(double price, int userId);

    double getUserBalanceByUserId(int userId);
}
