package com.miroliubov.application.dao;

import com.miroliubov.application.entity.Roles;
import com.miroliubov.application.entity.User;
import com.miroliubov.application.service.UserManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class UserDao implements IUserDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private UserManager userManager;

    @Override
    public void createUser(User user) {
        String sql = "INSERT INTO `rentaldb`.`user` (`email`, `password`, `name`, `surname`, `role`, `balance`) VALUES (?, ?, ?, ?, ?, ?);";
        jdbcTemplate.update(sql, user.getEmail(), user.getPassword(),
                user.getName(), user.getSurName(), Roles.USER.toString(), user.getBalance());
    }

    @Override
    public User getUserByEmail(String email) {
        String sql = "SELECT * FROM rentaldb.user WHERE email = ?";
        return jdbcTemplate.queryForObject(sql, new Object[]{email}, ROW_MAPPER);
    }

    @Override
    public void decreaseBalance(double price, int userId) {
        String sql = "UPDATE rentaldb.user SET balance='" + price + "' WHERE user_id='" + userId + "';";
        jdbcTemplate.update(sql);
    }

    @Override
    public double getUserBalanceByUserId(int userId) {

        String sql = "SELECT user.balance FROM rentaldb.user WHERE user_id='" + userId + "';";

        return jdbcTemplate.queryForObject(sql, Double.class);
    }

    private RowMapper<User> ROW_MAPPER = (resultSet, rowNumber) ->
            new User(
                    resultSet.getInt("user_id"),
                    resultSet.getString("email"),
                    resultSet.getString("password"),
                    resultSet.getString("name"),
                    resultSet.getString("surname"),
                    resultSet.getString("role"),
                    resultSet.getDouble("balance"));
}
