package com.miroliubov.application.dao;

import com.miroliubov.application.entity.Car;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Map;

@Repository
public class CarDao implements ICarDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public void createCar(Car car) {
        String sql = "INSERT INTO `rentaldb`.`cars` (`image`, `model`, `price`) VALUES (?, ?, ?);";
        jdbcTemplate.update(sql, car.getImage(), car.getModel(), car.getPrice());
    }

    @Override
    public List<Map<String, Object>> getAllCars() {

        String sql = "SELECT image, model, price FROM rentaldb.cars;";
        List<Map<String, Object>> rs = jdbcTemplate.queryForList(sql);
        return rs;
    }

    @Override
    public Car getCarByModel(String model) {
        String sql = "SELECT car_id, image, model, price FROM rentaldb.cars WHERE model = ?;";
        return jdbcTemplate.queryForObject(sql, new Object[]{model}, ROW_MAPPER);
    }

    private RowMapper<Car> ROW_MAPPER = (resultSet, rowNumber) ->
            new Car(
                    resultSet.getInt("car_id"),
                    resultSet.getString("image"),
                    resultSet.getString("model"),
                    resultSet.getDouble("price"));
}
