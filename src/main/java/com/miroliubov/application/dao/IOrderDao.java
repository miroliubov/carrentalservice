package com.miroliubov.application.dao;

import com.miroliubov.application.entity.Order;
import com.miroliubov.application.entity.RepairOrder;
import com.miroliubov.application.entity.TableOrder;

import java.util.List;

public interface IOrderDao {


    void createOrder(Order order);

    List<TableOrder> getOrderByUserId(int userId);

    int getPeriodsNumbers(String date1, String date2, int i);

    void changeStatusById(int orderId, String toString);

    List<TableOrder> getOrderByStatus(String toString);

    void deleteOrderByOrderId(int orderId);

    void setBrokenStatus(int orderId);

    void setRepairOption(int orderId, String textrepair, double price);

    List<RepairOrder> getRepairOrderByUserId(int userId);

    double getRepairOrderByOrderId(int orderId);

    void deleteOrderInOrderedCars(int orderId);
}
