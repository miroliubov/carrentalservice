package com.miroliubov.application.dao;

import com.miroliubov.application.entity.Order;
import com.miroliubov.application.entity.RepairOrder;
import com.miroliubov.application.entity.Statuses;
import com.miroliubov.application.entity.TableOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class OrderDao implements IOrderDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public void createOrder(Order order) {
        String sql = "INSERT INTO `rentaldb`.`order` (`car_id`, `start_date`, `finish_date`, " +
                "`passport_number`, `user_id`, `user_balance`, `status`) VALUES (?, ?, ?, ?, ?, ?, ?);";
        jdbcTemplate.update(sql, order.getCarId(), order.getStartDate(), order.getFinishDate(), order.getPassport(),
                order.getUserId(), order.getUserBalance(), Statuses.RESERVED.toString());
    }

    @Override
    public List<TableOrder> getOrderByUserId(int userId) {
        String sql = "SELECT `order`.order_id, `order`.start_date, `order`.finish_date, `order`.status, `cars`.model, `cars`.price \n" +
                "FROM rentaldb.`order`, rentaldb.`cars`\n" +
                "WHERE `cars`.car_id = `order`.car_id AND user_id = '" + userId +"';";
        return jdbcTemplate.query(sql, ROW_MAPPER);
    }

    @Override
    public int getPeriodsNumbers(String date1, String date2, int car_id) {

        String sql = "SELECT COUNT(*) FROM rentaldb.order \n" +
                "WHERE car_id='" + car_id + "' \n" +
                "AND ((start_date BETWEEN str_to_date('" + date1 + "', '%Y-%m-%d') AND str_to_date('" + date2 + "', '%Y-%m-%d')) \n" +
                "OR (finish_date BETWEEN str_to_date('" + date1 + "', '%Y-%m-%d') AND str_to_date('" + date2 + "', '%Y-%m-%d')) \n" +
                "OR (start_date < str_to_date('" + date1 + "', '%Y-%m-%d') AND finish_date > str_to_date('" + date2 + "', '%Y-%m-%d')));";


        return jdbcTemplate.queryForObject(sql, Integer.class);

    }

    @Override
    public void changeStatusById(int orderId, String status) {
        String sql = "UPDATE rentaldb.order SET status='" + status + "' WHERE order_id='" + orderId + "';";
        jdbcTemplate.update(sql);
    }

    @Override
    public List<TableOrder> getOrderByStatus(String status) {

        String sql = "SELECT * FROM rentaldb.order WHERE status = '" + status + "';";
        return jdbcTemplate.query(sql, ROW_MAPPER_WAIT);

    }

    @Override
    public void deleteOrderByOrderId(int orderId) {
        String sql = "delete from rentaldb.order where order_id='" + orderId + "'";
        jdbcTemplate.update(sql);
    }

    @Override
    public void setBrokenStatus(int orderId) {
        String sql = "UPDATE rentaldb.order \n" +
                "SET status='BROKEN' \n" +
                "WHERE order_id='" + orderId + "'";

        jdbcTemplate.update(sql);
    }

    @Override
    public void setRepairOption(int orderId, String textrepair, double price) {
        String sql = "INSERT INTO `rentaldb`.`orderedcars` (`order_id`, `textreason`, `repairprice`) VALUES (?, ?, ?);";
        jdbcTemplate.update(sql, orderId, textrepair, price);
    }

    @Override
    public List<RepairOrder> getRepairOrderByUserId(int userId) {

        String sql = "SELECT cars.model, orderedcars.repairprice, orderedcars.textreason, order.order_id " +
                "FROM rentaldb.cars, rentaldb.orderedcars, rentaldb.order WHERE order.car_id = cars.car_id " +
                "AND (order.order_id = orderedcars.order_id AND order.user_id = '" + userId + "');";

        return jdbcTemplate.query(sql, ROW_MAPPER_REPAIR);
    }

    @Override
    public double getRepairOrderByOrderId(int orderId) {
        String sql = "SELECT orderedcars.repairprice FROM rentaldb.orderedcars WHERE order_id='" + orderId + "';";
        return jdbcTemplate.queryForObject(sql, Double.class);
    }

    @Override
    public void deleteOrderInOrderedCars(int orderId) {
        String sql = "delete from rentaldb.orderedcars where order_id='" + orderId + "'";
        jdbcTemplate.update(sql);
    }


    private RowMapper<TableOrder> ROW_MAPPER = (resultSet, rowNumber) ->
            new TableOrder(
                    resultSet.getInt("order_id"),
                    resultSet.getString("model"),
                    resultSet.getDouble("price"),
                    resultSet.getDate("start_date").toLocalDate(),
                    resultSet.getDate("finish_date").toLocalDate(),
                    resultSet.getString("status"));

    private RowMapper<TableOrder> ROW_MAPPER_WAIT = (resultSet, rowNumber) ->
            new TableOrder(
                    resultSet.getInt("car_id"),
                    resultSet.getDate("start_date").toLocalDate(),
                    resultSet.getDate("finish_date").toLocalDate(),
                    resultSet.getString("status"),
                    resultSet.getInt("order_id"),
                    resultSet.getInt("user_id"));

    private RowMapper<RepairOrder> ROW_MAPPER_REPAIR = (resultSet, rowNumber) ->
            new RepairOrder(
                    resultSet.getString("model"),
                    resultSet.getDouble("repairprice"),
                    resultSet.getString("textreason"),
                    resultSet.getInt("order_id"));
}
