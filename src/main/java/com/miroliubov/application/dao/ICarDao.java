package com.miroliubov.application.dao;

import com.miroliubov.application.entity.Car;

import java.util.List;
import java.util.Map;

public interface ICarDao {

    public void createCar(Car car);

    public List<Map<String, Object>> getAllCars();

    Car getCarByModel(String model);
}
