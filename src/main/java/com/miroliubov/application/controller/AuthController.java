package com.miroliubov.application.controller;

import com.miroliubov.application.entity.User;
import com.miroliubov.application.service.IUserService;
import com.miroliubov.application.service.UserManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;


@Controller
public class AuthController {

    @Autowired
    private UserManager userManager;

    @Autowired
    private IUserService userService;


    /*Контроллеры для страницы логина*/

    /*Получение страницы формы логина*/
    @GetMapping("/login")
    public ModelAndView getLoginPage(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("login-page");
        return modelAndView;
    }

    /*Редирект на главную стрницу после успешного логина*/
    @PostMapping("/login")
    @ResponseBody
    public ModelAndView login(@RequestParam("email") String email, @RequestParam("password") String password) {
        ModelAndView modelAndView = new ModelAndView();
        if (userService.validateLogin(email, password).equals("access")) {
            userManager.setUser(userService.getUserByEmailService(email));
            if (userManager.getUser().getRole().toString().equals("ADMIN")) {
                modelAndView.setViewName("redirect:/admin");
            }
            else {
                modelAndView.setViewName("redirect:/home");
            }
        } else {
            modelAndView.addObject("loginerror", "login or password incorrect");
        }

        return modelAndView;
    }

    /*Получение страницы формы регистрации*/
    @GetMapping("/registration")
    public ModelAndView getRegistrationPage() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("registration-page");
        return modelAndView;
    }

    /*Редирект на главную стрницу после успешной регистрации*/
    @PostMapping("/registration")
    @ResponseBody
    public ModelAndView registration(@Validated User user) {
        userService.addNewUser(user);

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/login-page");
        return modelAndView;
    }

    @RequestMapping("/logout")
    public String logout(HttpServletRequest request) {
        request.getSession().invalidate();
        return "login-page";
    }



}
