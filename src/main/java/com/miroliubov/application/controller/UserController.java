package com.miroliubov.application.controller;

import com.miroliubov.application.entity.RepairOrder;
import com.miroliubov.application.entity.TableOrder;
import com.miroliubov.application.entity.User;
import com.miroliubov.application.service.IOrderService;
import com.miroliubov.application.service.UserManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import java.util.List;


@Controller
public class UserController {

    @Autowired
    private UserManager userManager;

    @Autowired
    private IOrderService orderService;

    @RequestMapping("/status")
    public ModelAndView orderStatus () {
        ModelAndView modelAndView = new ModelAndView();
        User currentUser = userManager.getUser();
        String name = currentUser.getName() + " " + currentUser.getSurName();
        modelAndView.addObject("userName", name);
        modelAndView.addObject("userBalance", currentUser.getBalance());
        List<TableOrder> order = orderService.getOrderForm(currentUser.getUserId());
        modelAndView.addObject("orders", order);

        List<RepairOrder> repairOrders = orderService.getRepairOrders(currentUser.getUserId());
        modelAndView.addObject("repairOrders", repairOrders);

        modelAndView.setViewName("order-status");
        return modelAndView;
    }

    @PostMapping("/status")
    @ResponseBody
    public ModelAndView receiveCar(@RequestParam("orderId") int orderId) {
        orderService.changeOrderWaitStatus(orderId);
        ModelAndView modelAndView = new ModelAndView();
        User currentUser = userManager.getUser();
        String name = currentUser.getName() + " " + currentUser.getSurName();
        modelAndView.addObject("userName", name);
        modelAndView.addObject("userBalance", currentUser.getBalance());
        List<TableOrder> order = orderService.getOrderForm(currentUser.getUserId());

        List<RepairOrder> repairOrders = orderService.getRepairOrders(currentUser.getUserId());
        modelAndView.addObject("repairOrders", repairOrders);

        modelAndView.addObject("orders", order);
        modelAndView.setViewName("redirect:/status");
        return modelAndView;
    }

}
