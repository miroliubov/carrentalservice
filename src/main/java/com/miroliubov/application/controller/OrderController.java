package com.miroliubov.application.controller;

import com.miroliubov.application.entity.TableOrder;
import com.miroliubov.application.service.IOrderService;
import com.miroliubov.application.service.IUserService;
import com.miroliubov.application.service.UserManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import java.time.LocalDate;
import java.util.List;

@Controller
public class OrderController {

    @Autowired
    private IOrderService orderService;

    @Autowired
    private UserManager userManager;

    @Autowired
    private IUserService userService;

    @GetMapping("/adminorder")
    public ModelAndView adminConfirmationPage() {
        ModelAndView modelAndView = new ModelAndView();

        List<TableOrder> order = orderService.getWaitOrders();

        modelAndView.addObject("orders", order);

        modelAndView.setViewName("admin-status");
        return modelAndView;
    }

    @PostMapping("/home")
    @ResponseBody
    public ModelAndView addNewOrder(@RequestParam("models") String model, @RequestParam("date1")
            String date1, @RequestParam("date2") String date2, @RequestParam("passport") int passport) {

        ModelAndView modelAndView = new ModelAndView();

        LocalDate startDate = LocalDate.parse(date1);

        LocalDate finishDate = LocalDate.parse(date2);

        if (startDate.isAfter(finishDate)) {
            modelAndView.addObject("error", "Неправильное время");
            modelAndView.setViewName("error-page");
            return modelAndView;
        }

        boolean isExist = orderService.checkExistOrderDate(date1, date2, model);

        if (!isExist) {
            modelAndView.addObject("error", "Время занято");
            modelAndView.setViewName("error-page");
            return modelAndView;
        }

        if (orderService.checkBalance(startDate, finishDate, model)) {
            orderService.createNewOrder(model, date1, date2, passport);
        }

        modelAndView.addObject("okMsg", "Успешно! Следите за статусом заказа.");
        modelAndView.setViewName("redirect:/home");
        return modelAndView;
    }

    @PostMapping("/adminorder")
    @ResponseBody
    public ModelAndView returnOrderStatus(@RequestParam("status") String status, @RequestParam("textrepair")
            String textrepair, @RequestParam("repair") String repairPrice, @RequestParam("orderId") int orderId) {
        ModelAndView modelAndView = new ModelAndView();

        if (status.equals("RETURN")) {
            orderService.returnCar(orderId);
        }

        if (status.equals("BROKEN")) {
            if (repairPrice.equals("")) {
                modelAndView.addObject("error", "Не заполнена строка стоимости");
                modelAndView.setViewName("error-page");
                return modelAndView;
            }
            orderService.carIsBroken(orderId, repairPrice, textrepair);
        }

        List<TableOrder> order = orderService.getWaitOrders();
        modelAndView.addObject("orders", order);

        modelAndView.setViewName("redirect:/adminorder");
        return modelAndView;
    }

    @PostMapping("/repair")
    @ResponseBody
    public ModelAndView paymentOfRepair(@RequestParam("orderId") int orderId) {

        ModelAndView modelAndView = new ModelAndView();

        boolean isEnough = orderService.checkEnoughMoney(userManager.getUser().getUserId(), orderId);

        if (!isEnough) {
            modelAndView.addObject("error", "Не хватает денег");
            modelAndView.setViewName("error-page");
            return modelAndView;
        }

        /* Здесь уменьшить баланс */

        double newBalance = userManager.getUser().getBalance() - orderService.getRepairBalance(orderId);

        userService.decreaseBalanceByUserId(newBalance, userManager.getUser().getUserId());
        userManager.getUser().setBalance(newBalance);

        orderService.deleteOrder(orderId);

        modelAndView.setViewName("redirect:/status");

        return modelAndView;
    }

}
