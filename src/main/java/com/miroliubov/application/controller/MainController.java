package com.miroliubov.application.controller;

import com.miroliubov.application.entity.User;
import com.miroliubov.application.service.ICarService;
import com.miroliubov.application.service.IOrderService;
import com.miroliubov.application.service.UserManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import java.util.*;


@Controller
public class MainController {

    @Autowired
    private UserManager userManager;

    @Autowired
    private ICarService carService;

    @Autowired
    private IOrderService orderService;

    @RequestMapping("/home")
    public ModelAndView homePage() {
        ModelAndView modelAndView = new ModelAndView();
        Map<String, String> cars = carService.getModels();
        modelAndView.addObject("models", cars);
        modelAndView.setViewName("home-page");
        User currentUser = userManager.getUser();
        modelAndView.addObject("currentUserRole", currentUser.getRole());
        String name = currentUser.getName() + " " + currentUser.getSurName();
        modelAndView.addObject("userName", name);
        modelAndView.addObject("userBalance", currentUser.getBalance());

        return modelAndView;
    }

    @GetMapping("/admin")
    public ModelAndView adminLogin() {
        ModelAndView modelAndView = new ModelAndView();

        if (userManager.getUser().getRole().toString().equals("ADMIN")) {
            modelAndView.setViewName("admin-page");
        } else {
            modelAndView.setViewName("redirect:/home");
        }
        return modelAndView;
    }

    @PostMapping("/admin")
    @ResponseBody
    public ModelAndView addCarInTable(@RequestParam("model") String model, @RequestParam("price") String price,
                              @RequestParam("img") String img) {
        carService.addNewCar(model, price, img);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/admin");
        return modelAndView;
    }

}
