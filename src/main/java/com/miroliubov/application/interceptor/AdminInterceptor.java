package com.miroliubov.application.interceptor;

import com.miroliubov.application.entity.Roles;
import com.miroliubov.application.entity.User;
import com.miroliubov.application.service.UserManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Objects;

public class AdminInterceptor implements HandlerInterceptor {

    @Autowired
    private UserManager userManager;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
        User currentUser = userManager.getUser();

        if (Objects.isNull(currentUser) || currentUser.getRole().equals(Roles.USER)) {
            response.sendRedirect("/home");
            return false;
        }

        return true;
    }
}
