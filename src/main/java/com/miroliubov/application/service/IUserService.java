package com.miroliubov.application.service;

import com.miroliubov.application.entity.User;

public interface IUserService {

    void addNewUser(User user);

    String validateLogin(String email, String password);

    User getUserByEmailService(String email);

    void decreaseBalanceByUserId(double price, int userId);

    double getUserBalanceByUserId(int userId);
}
