package com.miroliubov.application.service;

import com.miroliubov.application.dao.ICarDao;
import com.miroliubov.application.entity.Car;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CarService implements ICarService {

    @Autowired
    private ICarDao carDao;

    @Override
    public void addNewCar(String model, String price, String img) {

        if (model != null && price != null && img != null) {
            Car car;
            try {
                double carPrice = Double.parseDouble(price);
                car = new Car(model, carPrice, img);
                carDao.createCar(car);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("oooops");
        }

    }

    @Override
    public List<Map<String, Object>> getAllCars(){
        return carDao.getAllCars();
    }

    @Override
    public Map<String, String> getModels() {
        List<Map<String, Object>> rs = getAllCars();
        List<String> models = new ArrayList<>();
        for (Map<String, Object> r : rs) {
            models.add(r.get("model").toString());
        }
        Map<String, String> cars = new HashMap<>();
        for (int i = 0; i < models.size(); i++) {
            cars.put(models.get(i), models.get(i));
        }
        return cars;
    }

    @Override
    public int getCarIdByModel(String model) {

        return carDao.getCarByModel(model).getCarId();

    }

    @Override
    public double getCarPriceByModel(String model) {
        return carDao.getCarByModel(model).getPrice();
    }

}
