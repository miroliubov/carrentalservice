package com.miroliubov.application.service;

import com.miroliubov.application.dao.IUserDao;
import com.miroliubov.application.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService implements IUserService {

    @Autowired
    private UserManager userManager;

    @Autowired
    private IUserDao userDao;

    @Override
    public void addNewUser(User user) {
        userDao.createUser(user);
    }

    @Override
    public User getUserByEmailService(String email) {
        return userDao.getUserByEmail(email);
    }

    @Override
    public void decreaseBalanceByUserId(double price, int userId) {
        userDao.decreaseBalance(price, userId);
    }

    @Override
    public double getUserBalanceByUserId(int userId) {
        return userDao.getUserBalanceByUserId(userId);
    }

    @Override
    public String validateLogin(String email, String password) {
        User user = userDao.getUserByEmail(email);
        if (user.getPassword().equals(password)) {
            return "access";
        } else {
            return "deny";
        }
    }
}
