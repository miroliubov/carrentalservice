package com.miroliubov.application.service;

import com.miroliubov.application.entity.RepairOrder;
import com.miroliubov.application.entity.TableOrder;

import java.time.LocalDate;
import java.util.List;

public interface IOrderService {
    void createNewOrder(String model, String date1, String date2, int passport);

    List<TableOrder> getOrderForm(int userId);

    boolean checkExistOrderDate(String date1, String date2, String model);

    boolean checkBalance(LocalDate startDate, LocalDate finishDate, String model);

    void changeOrderWaitStatus(int orderId);

    List<TableOrder> getWaitOrders();

    void returnCar(int orderId);

    void carIsBroken(int orderId, String repairPrice, String textrepair);

    List<RepairOrder> getRepairOrders(int userId);

    boolean checkEnoughMoney(int userId, int orderId);

    double getRepairBalance(int orderId);

    void deleteOrder(int orderId);
}
