package com.miroliubov.application.service;

import com.miroliubov.application.dao.IOrderDao;
import com.miroliubov.application.entity.Order;
import com.miroliubov.application.entity.RepairOrder;
import com.miroliubov.application.entity.Statuses;
import com.miroliubov.application.entity.TableOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.Period;
import java.util.List;

@Service
public class OrderService implements IOrderService {

    @Autowired
    private IOrderDao orderDao;

    @Autowired
    private UserManager userManager;

    @Autowired
    private ICarService carService;

    @Autowired
    private IUserService userService;

    @Override
    public void createNewOrder(String model, String date1, String date2, int passport) {

        LocalDate startDate = LocalDate.parse(date1);

        LocalDate finishDate = LocalDate.parse(date2);

        int userId = userManager.getUser().getUserId();

        double price = carService.getCarPriceByModel(model);
        Period period = Period.between(startDate, finishDate);
        Integer daysElapsed = period.getDays();
        double periodPrice = (daysElapsed + 1) * price;

        double userBalance = userManager.getUser().getBalance();

        double balance = userBalance - periodPrice;

        userService.decreaseBalanceByUserId(balance, userId);

        int carId = carService.getCarIdByModel(model);

        Order order = new Order(startDate, finishDate, passport, carId, userId);

        orderDao.createOrder(order);

        userManager.getUser().setBalance(balance);

    }

    @Override
    public List<TableOrder> getOrderForm(int userId) {

        List<TableOrder> orderByUserId = orderDao.getOrderByUserId(userId);

        for (TableOrder tableOrder : orderByUserId) {
            Period period = Period.between(tableOrder.getStart_date(), tableOrder.getFinish_date());
            Integer daysElapsed = period.getDays();
            double price = (daysElapsed + 1) * tableOrder.getPrice();
            tableOrder.setPeriodPrice(price);
        }

        return orderByUserId;

    }

    @Override
    public boolean checkExistOrderDate(String date1, String date2, String model) {

        int carId = carService.getCarIdByModel(model);

        int countPeriod = orderDao.getPeriodsNumbers(date1, date2, carId);

        return countPeriod == 0;
    }

    @Override
    public boolean checkBalance(LocalDate startDate, LocalDate finishDate, String model) {

        double price = carService.getCarPriceByModel(model);
        Period period = Period.between(startDate, finishDate);
        Integer daysElapsed = period.getDays();
        double periodPrice = (daysElapsed + 1) * price;

        return userManager.getUser().getBalance() > periodPrice;
    }

    @Override
    public void changeOrderWaitStatus(int orderId) {
        orderDao.changeStatusById(orderId, Statuses.WAIT.toString());
    }

    @Override
    public List<TableOrder> getWaitOrders() {

        List<TableOrder> list = orderDao.getOrderByStatus(Statuses.WAIT.toString());

        return list;
    }

    @Override
    public void returnCar(int orderId) {
        orderDao.deleteOrderByOrderId(orderId);
    }

    /*Изменение стутуса на BROKEN, добавление сообщения о ремонте*/
    @Override
    public void carIsBroken(int orderId, String repairPrice, String textrepair) {

        orderDao.setBrokenStatus(orderId);

        double price = Double.parseDouble(repairPrice);

        orderDao.setRepairOption(orderId, textrepair, price);

    }

    @Override
    public List<RepairOrder> getRepairOrders(int userId) {

        List<RepairOrder> repairOrders = orderDao.getRepairOrderByUserId(userId);

        return repairOrders;
    }

    @Override
    public boolean checkEnoughMoney(int userId, int orderId) {

        double userBalance = userService.getUserBalanceByUserId(userId);

        double repairPrice = orderDao.getRepairOrderByOrderId(orderId);

        return userBalance >= repairPrice;

    }

    @Override
    public double getRepairBalance(int orderId) {

        return orderDao.getRepairOrderByOrderId(orderId);
    }

    @Override
    public void deleteOrder(int orderId) {
        orderDao.deleteOrderByOrderId(orderId);
        orderDao.deleteOrderInOrderedCars(orderId);
    }

}
