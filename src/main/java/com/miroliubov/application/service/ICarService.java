package com.miroliubov.application.service;

import java.util.List;
import java.util.Map;

public interface ICarService {

    void addNewCar(String model, String price, String img);

    List<Map<String, Object>> getAllCars();

    Map<String, String> getModels();

    int getCarIdByModel(String model);

    double getCarPriceByModel(String model);
}
