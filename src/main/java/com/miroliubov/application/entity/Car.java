package com.miroliubov.application.entity;

public class Car {

    private int carId;

    private String model;

    private double price;

    private String image;

    public Car() {
        super();
    }

    public Car(String model, double price, String image) {
        this.model = model;
        this.price = price;
        this.image = image;
    }

    public Car(int carId, String image, String model, double price) {
        this.carId = carId;
        this.image = image;
        this.model = model;
        this.price = price;
    }

    public int getCarId() {
        return carId;
    }

    public void setCarId(int carId) {
        this.carId = carId;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
