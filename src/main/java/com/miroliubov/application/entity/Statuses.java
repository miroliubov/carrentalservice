package com.miroliubov.application.entity;

public enum Statuses {
    DENIED, ALLOWED, RETURN, BROKEN, WAIT, RESERVED
}
