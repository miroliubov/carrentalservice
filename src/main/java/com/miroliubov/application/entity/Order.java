package com.miroliubov.application.entity;

import java.time.LocalDate;
import java.util.Date;

public class Order {

    private LocalDate startDate;

    private LocalDate finishDate;

    private int passport;

    private int carId;

    private int userId;

    private double userBalance;

    public Order(LocalDate startDate, LocalDate finishDate, int passport, int carId, int userId) {
        this.startDate = startDate;
        this.finishDate = finishDate;
        this.passport = passport;
        this.carId = carId;
        this.userId = userId;

    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(LocalDate finishDate) {
        this.finishDate = finishDate;
    }

    public int getPassport() {
        return passport;
    }

    public void setPassport(int passport) {
        this.passport = passport;
    }

    public int getCarId() {
        return carId;
    }

    public void setCarId(int carId) {
        this.carId = carId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public double getUserBalance() {
        return userBalance;
    }

    public void setUserBalance(double userBalance) {
        this.userBalance = userBalance;
    }
}
