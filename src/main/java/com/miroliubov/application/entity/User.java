package com.miroliubov.application.entity;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class User {

    @NotEmpty(message="Необходимо заполнить поле почты")
    @Email(message="Введите корректный адрес почты")
    private String email;

    @NotEmpty
    @Size(min = 6, max = 15)
    private String password;

    private String name;

    private String surName;

    private Enum role;

    private double balance;

    private int userId;

    public User() {
        super();
    }

    public User(@NotEmpty(message = "Необходимо заполнить поле почты")
                @Email(message = "Введите корректный адрес почты") String email,
                @NotEmpty @Size(min = 6, max = 15) String password, String name,
                String surName, String role, double balance) {
        this.email = email;
        this.password = password;
        this.name = name;
        this.surName = surName;
        this.role = Roles.valueOf(role);
        this.balance = balance;
    }

    public User(int userId,
                @NotEmpty(message = "Необходимо заполнить поле почты")
                @Email(message = "Введите корректный адрес почты") String email,
                @NotEmpty @Size(min = 6, max = 15) String password, String name,
                String surName, String role, double balance) {
        this.email = email;
        this.password = password;
        this.name = name;
        this.surName = surName;
        this.role = Roles.valueOf(role);;
        this.balance = balance;
        this.userId = userId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public Enum getRole() {
        return role;
    }

    public void setRole(Enum role) {
        this.role = role;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
