package com.miroliubov.application.entity;

import java.time.LocalDate;

public class TableOrder {

    private String model;

    private int carId;

    private Double price;

    private LocalDate start_date;

    private LocalDate finish_date;

    private String status;

    private Double periodPrice;

    private int orderId;

    private int userId;

    public TableOrder(int orderId, String model, Double price, LocalDate start_date, LocalDate finish_date, String status) {
        this.orderId = orderId;
        this.model = model;
        this.price = price;
        this.start_date = start_date;
        this.finish_date = finish_date;
        this.status = status;
    }

    public TableOrder(int carId, LocalDate start_date, LocalDate finish_date, String status, int orderId, int userId) {
        this.carId = carId;
        this.start_date = start_date;
        this.finish_date = finish_date;
        this.status = status;
        this.orderId = orderId;
        this.userId = userId;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public LocalDate getStart_date() {
        return start_date;
    }

    public void setStart_date(LocalDate start_date) {
        this.start_date = start_date;
    }

    public LocalDate getFinish_date() {
        return finish_date;
    }

    public void setFinish_date(LocalDate finish_date) {
        this.finish_date = finish_date;
    }

    public Double getPeriodPrice() {
        return periodPrice;
    }

    public void setPeriodPrice(Double periodPrice) {
        this.periodPrice = periodPrice;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getCarId() {
        return carId;
    }

    public void setCarId(int carId) {
        this.carId = carId;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }
}
