package com.miroliubov.application.entity;

public class RepairOrder {

    private String model;

    private String message;

    private double repairPrice;

    private int orderId;

    public RepairOrder(String model, double repairPrice, String message, int orderId) {
        this.model = model;
        this.repairPrice = repairPrice;
        this.message = message;
        this.orderId = orderId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public double getRepairPrice() {
        return repairPrice;
    }

    public void setRepairPrice(double repairPrice) {
        this.repairPrice = repairPrice;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }
}
